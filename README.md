# MeetupSpontedatascience

Meetup sobre Data Science realizado na Sponte em 03 de Maio de 2019

O conteúdo pre-supõe que você possui o ambiente configurado e as libs instaladas, caso não tenha isso ainda  instale o https://jupyter.org/install e os as libs da ultima
página do .pdf ;) ou execute pip install -R Requeriments.txt "For linux"

Para entender melhor o jupyter abra e olhe os aquivos na seguinte ordem:

Meetup Sponte analise explorativa.ipynb

Meetup Sponte  visualizacao de dados.ipynb

Meetup Sponte analise preditiva.ipnb

Os datasets .CSV são arquivos dispiníveis no https://www.kaggle.com/datasets

Enjoy ;)
